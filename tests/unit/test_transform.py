import transform


def test_init_type():
    assert transform.init_value(int) == 0
    assert transform.init_value(float) == 0.0
    assert transform.init_value(str) == ''
    assert transform.init_value(list) == []
    assert transform.init_value(dict) == {}


def test_log():
    log = {"ad_network": "FOO",
           "date": "2019-06-05",
           "app_name": "LINETV",
           "unit_id": "55665201314",
           "request": "100",
           "revenue": "0.00365325",
           "imp": "23"}

    result_data = transform.transform_log(log)

    assert 'unit_id' in result_data.keys()
    assert 'revenue' in result_data.keys()
    assert result_data['date'] is not None
    assert isinstance(result_data['revenue'], float)


def test_lose_request_data_log():
    log = {"ad_network": "FOO",
           "date": "2019-06-05",
           "app_name": "LINETV",
           "unit_id": "55665201314",
           "request": "",
           "revenue": "0.00365325",
           "imp": "23"}

    result_data = transform.transform_log(log)

    assert 'request' in result_data.keys()
    assert result_data['request'] is not None
    assert result_data['request'] == 0


def test_lose_revenue_data_log():
    log = {"ad_network": "FOO",
           "date": "2019-06-05",
           "app_name": "LINETV",
           "unit_id": "55665201314",
           "request": "23",
           "imp": "23"}

    result_data = transform.transform_log(log)

    assert 'revenue' in result_data.keys()
    assert result_data['revenue'] is not None
    assert result_data['revenue'] == 0.0


def test_log_date_error_type():
    log = {"ad_network": "FOO",
           "date": "2019-0605",
           "app_name": "LINETV",
           "unit_id": "55665201314",
           "request": "",
           "revenue": "0.00365325",
           "imp": "23"}

    result_data = transform.transform_log(log)

    assert 'date' in result_data.keys()
    assert result_data['date'] is None
