import datetime as dt


def init_value(val_type: classmethod) -> object:
    """
    Get init data by type
    Args:
        val_type: value's type, e.g. int, str, list, dict

    Returns:
        data: init value
    """

    if val_type == int:
        return 0
    elif val_type == str:
        return ''
    elif val_type == dt.date:
        return None
    elif val_type == float:
        return 0.0
    elif val_type == dict:
        return {}
    elif val_type == list:
        return []
    else:
        return None


def transform_log(data: dict) -> dict:
    """
    Transform log data
    Args:
        data: {"ad_network": "str",
               "date": "date",
               "app_name": "str",
               "unit_id": "str",
               "request": "int",
               "revenue": "float",
               "imp": "int"}

    Returns:
        dict: {"ad_network": str,
               "date": date,
               "app_name": str,
               "unit_id": str,
               "request": int,
               "revenue": float,
               "imp": int}

    """
    column_define = {"ad_network": str,
                     "date": dt.date,
                     "app_name": str,
                     "unit_id": str,
                     "request": int,
                     "revenue": float,
                     "imp": int}

    final_data = {}

    for d in column_define.items():
        data_col = d[0]
        data_type = d[1]

        try:
            if data_col in data:
                data_value = data[data_col]

                if data_type == dt.date:
                    # 目前設定的預期時間格式為 yyyy-MM-dd
                    final_data[data_col] = dt.datetime.strptime(data_value, '%Y-%m-%d').date()
                elif isinstance(data_value, data_type):
                    final_data[data_col] = data_type(data_value)
                else:
                    final_data[data_col] = init_value(data_type)
            else:
                final_data[data_col] = init_value(data_type)
        except Exception as error:
            print(error)
            final_data[data_col] = init_value(data_type)

    return final_data
